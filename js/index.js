class Hangman {
    constructor() {
        this.allWords = "https://bitbucket.org/ShravanKarnati/hangman/raw/4401c61ef0d96d8db86d0eec927d5f84cde35f9f/words(scraped)/data.json";
        this.chances = 5;
        this.svg = ['head', 'spine', 'hands', 'legs', "door"];   
    }

    hangmanDied = () => {

        $("#animate").click(function(){
            dropBody();
            $("#rEyes").addClass("hide");
            $("#xEyes").removeClass("hide");
        });

        function dropBody () {
          $("#door1").velocity({rotateZ: 90}, 1000);
          $("#door2").velocity({rotateZ: -90}, 1000);
          fall();  
        }


        function fall() {
          let dur = 500;
          let del = 1000;
          $("#body").velocity({translateY: "200px"}, {duration: dur, delay: del});
          $("#rope").velocity({y2: "+=200px"}, {duration: dur, delay: del});
          $("#armL").velocity({y2: "-=60px"}, {duration: dur, delay: del});
          $("#armR").velocity({y2: "-=60px"}, {duration: dur, delay: del});

          finish();
        }

        function finish () {
          $("#armL").velocity({y2: "+=70px", x2: "+=10px"}, 500);
          $("#armR").velocity({y2: "+=70px", x2: "-=10px"}, 500);
        }

        document.querySelector("#animate").click();

    };

    getWord = () => {
        fetch(this.allWords)
            .then(res => res.json())
            .then(data => {
                let randNum = Math.ceil(Math.random() * data.length);
                let newWord = data[randNum];
                console.log(newWord);
                this.makeBlank(newWord);
            })
            .catch(err => {
                document.querySelector(".reset").click();
            });
    }

    makeHTML = (word) => {
        let inputBox = document.querySelector(".input");
        let guess = document.querySelector(".guess");
        let wrongBucket = [];
        let a = word;
        guess.addEventListener("click", () => {
            if (inputBox.value.length == 1) {
                this.checkAlpha(inputBox.value, wrongBucket, a);
            }
            inputBox.value = "";
        })
    }

    disableGame = () => {

    }

    wrong = (letter, wrongBucket) => {
        if (!wrongBucket.includes(letter)) {
            this.chances--;
            this.svgDraw();
            if (this.chances === 0) {
                this.disableGame();
            };
        }
    }

    complete = () => {
        let allLetters = document.querySelectorAll(".letter-let");
        if (allLetters.length === 0) {
            console.log("completed");
        }
    }

    check = (letter) => {
        let allLetters = document.querySelectorAll(".letter-let");
        allLetters.forEach(cur => {
            if (cur.textContent == letter) {
                cur.classList.add("letter-shown");
                cur.classList.remove("letter-let");
            }
        })
    }

    checkAlpha = (alpha, wrongBucket, a) => {
        if (a.includes(alpha)) {
            this.check(alpha);
            this.complete();
        } else {
            this.wrong(alpha, wrongBucket);
        }
    }


    makeBlank = (word) => {
        let html = "";
        var regex = /^[a-zA-Z]+$/;
        for (let i = 0; i < word.length; i++) {
            (word[i].match(regex)) ? html += `<div class="letter"><p class="const letter-let">${word[i]}</p></div>`: html += `<div class="const blank">&nbsp;&nbsp;&nbsp;&nbsp;</div>`;
        }
        document.querySelector(".word").innerHTML = html;

        this.makeHTML(word);
    }

    svgDraw = () => {
        let now = this.svg[0];
        switch (now) {
            case "head":
                document.querySelectorAll(".head").forEach(cur => cur.classList.remove("hideSVG"));
                break;
            case "spine":
                document.querySelectorAll(".spine").forEach(cur => cur.classList.remove("hideSVG"));
                break;
            case "hands":
                document.querySelectorAll(".hands").forEach(cur => cur.classList.remove("hideSVG"));
                break;
            case "legs":
                document.querySelectorAll(".legs").forEach(cur => cur.classList.remove("hideSVG"));
                break;
            case "door":
                this.hangmanDied();
                break;
        }
        this.svg.shift();
    };
}


hangMan = new Hangman().getWord();